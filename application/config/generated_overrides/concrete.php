<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2015-06-10T16:41:24+00:00
 *
 * @item      misc.latest_version
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'locale' => 'da_DK',
    'site' => 'Danske Destinationer',
    'version_installed' => '5.7.3',
    'misc' => array(
        'access_entity_updated' => 1426711533,
        'latest_version' => '5.7.3.1',
        'seen_introduction' => true,
        'do_page_reindex_check' => false
    ),
    'cache' => array(
        'blocks' => false,
        'assets' => false,
        'theme_css' => false,
        'overrides' => false,
        'pages' => '0',
        'full_page_lifetime' => 'default',
        'full_page_lifetime_value' => null
    ),
    'theme' => array(
        'compress_preprocessor_output' => false
    ),
    'seo' => array(
        'url_rewriting' => 1
    ),
    'permissions' => array(
        'model' => 'advanced'
    ),
    'mail' => array(
        'method' => 'php_mail',
        'methods' => array(
            'smtp' => array(
                'server' => 'send.one.com',
                'username' => 'info@danskedestinationer.dk',
                'password' => 'dd2014',
                'port' => '',
                'encryption' => 'SSL'
            )
        )
    )
);
