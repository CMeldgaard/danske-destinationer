<?php

namespace Application\Block\PageBreaker;

use Concrete\Core\Block\BlockController;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends BlockController
{

    protected $btTable = "btPageBreaker";
    protected $btInterfaceWidth = "470";
    protected $btInterfaceHeight = "270";
    protected $btDefaultSet = 'basic';

    public function getBlockTypeName()
    {
        return t('Breaker');
    }

    public function validate($data)
    {
        $e = Core::make('error');
        if (!$data['text']) {
            $e->add(t('Du skal skrive en tekst'));
        }
		if (!$data['overskrift']) {
            $e->add(t('Du skal skrive en overskrift'));
        }
        return $e;
    }

    public function getBlockTypeDescription()
    {
        return t('Tilføj indhold');
    }

    public function save($data)
    {
        $data['booleanfield'] = intval($data['booleanfield']);
        parent::save($data);
    }
}
