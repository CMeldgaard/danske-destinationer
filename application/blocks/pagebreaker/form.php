<?php

defined('C5_EXECUTE') or die(_("Access Denied."));

?>

<div class="form-group">
    <label class="control-label" for="field1"><?=t('overskrift')?></label>
    <input type="text" class="form-control" name="overskrift" value="<?php echo $overskrift?>">
</div>

<div class="form-group">
    <label class="control-label" for="field1"><?=t('text')?></label><br>
	<textarea id="redactor-content" name="text" cols="55" rows="4"><?php echo  $text?></textarea>
</div>
