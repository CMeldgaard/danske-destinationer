<?php

namespace Application\Block\HexList;

use Concrete\Core\Block\BlockController;
use \Concrete\Core\Editor\LinkAbstractor;
use Core;
use Loader;
use \File;
use Page;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends BlockController
{

    protected $btTable = "btHexList";
    protected $btInterfaceWidth = "950";
    protected $btInterfaceHeight = "600";
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btSupportsInlineEdit = true;
	protected $btSupportsInlineAdd = true;
	protected $btCacheBlockOutputLifetime = 0; //until manually updated or cleared
    protected $btDefaultSet = 'basic';
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btWrapperClass = 'ccm-ui';
    protected $btExportFileColumns = array('fID');
    protected $btFeatures = array(
        'image'
    );

    public function getBlockTypeName()
    {
        return t('hex liste punkt');
    }
	
	function getContent() {
			return LinkAbstractor::translateFrom($this->content);
		}

		public function getSearchableContent(){
			return $this->content;
		}

		function br2nl($str) {
			$str = str_replace("\r\n", "\n", $str);
			$str = str_replace("<br />\n", "\n", $str);
			return $str;
		}

        public function registerViewAssets($outputContent)
        {
            if (preg_match('/data-concrete5-link-launch/i', $outputContent)) {
                $this->requireAsset('core/lightbox');
            }
        }

        public function view2()
        {
            $this->set('content', $this->getContent());
        }

		function getContentEditMode() {
			return LinkAbstractor::translateFromEditMode($this->content);
		}

		public function add() {
			$this->requireAsset('redactor');
            $this->requireAsset('core/file-manager');
		}

		public function edit() {
			$this->requireAsset('redactor');
            $this->requireAsset('core/file-manager');
		}

		public function composer() {
			$this->requireAsset('redactor');
            $this->requireAsset('core/file-manager');
		}

		public function getImportData($blockNode) {
			$content = $blockNode->data->record->content;
			$content = LinkAbstractor::import($content);
			$args = array('content' => $content);
			return $args;
		}

		public function export(\SimpleXMLElement $blockNode) {
			$data = $blockNode->addChild('data');
			$data->addAttribute('table', $this->btTable);
			$record = $data->addChild('record');
			$cnode = $record->addChild('content');
			$node = dom_import_simplexml($cnode);
			$no = $node->ownerDocument;
			$content = LinkAbstractor::export($this->content);
			$cdata = $no->createCDataSection($content);
			$node->appendChild($cdata);
		}
		
		//Image functions

    public function view() {
        $f = File::getByID($this->fID);
        if (!is_object($f) || !$f->getFileID()) {
            return false;
        }

        // onState image available
        $foS = $this->getFileOnstateObject();
        if (is_object($foS)) {
            $imgPath = array();
            $imgPath['hover'] = File::getRelativePathFromID($this->fOnstateID);
            $imgPath['default'] = File::getRelativePathFromID($this->fID);
            $this->set('imgPath', $imgPath);
            $this->set('foS', $foS);
        }

        $this->set('f', $f);
        $this->set('altText',$this->getAltText());
        $this->set('title',$this->getTitle());
        $this->set('linkURL',$this->getLinkURL());
    }

    public function getJavaScriptStrings() {
        return array(
            'image-required' => t('You must select an image.')
        );
    }

    public function isComposerControlDraftValueEmpty() {
        $f = $this->getFileObject();
        if (is_object($f) && !$f->isError()) {
            return false;
        }
        return true;
    }

    public function getImageFeatureDetailFileObject() {
        // i don't know why this->fID isn't sticky in some cases, leading us to query
        // every damn time
        $db = Loader::db();
        $fID = $db->GetOne('select fID from btContentImage where bID = ?', array($this->bID));
        if ($fID) {
            $f = File::getByID($fID);
            if (is_object($f) && !$f->isError()) {
                return $f;
            }
        }
    }

    function getFileID() {return $this->fID;}
    function getFileOnstateID() {return $this->fOnstateID;}
    public function getFileOnstateObject() {
        if($this->fOnstateID) {
            return File::getByID($this->fOnstateID);
        }
    }
    public function getFileObject() {
        return File::getByID($this->fID);
    }
    function getAltText() {return $this->altText;}
    function getTitle() {return $this->title;}
    function getExternalLink() {return $this->externalLink;}
    function getInternalLinkCID() {return $this->internalLinkCID;}
    function getLinkURL() {
        if (!empty($this->externalLink)) {
            return $this->externalLink;
        } else if (!empty($this->internalLinkCID)) {
            $linkToC = Page::getByID($this->internalLinkCID);
            return (empty($linkToC) || $linkToC->error) ? '' : Loader::helper('navigation')->getLinkToCollection($linkToC);
        } else {
            return '';
        }
    }
		
	
	//Block functions

    public function validate($data)
    {
        $e = Core::make('error');
        if (!$data['content']) {
            $e->add(t('Du skal skrive en tekst'));
        }
		if (!$data['headline']) {
            $e->add(t('Du skal indskrive en url'));
        }
		if (!$data['fID']) {
            $e->add(t('Du skal vælge et billede'));
        }
        return $e;
    }

    public function getBlockTypeDescription()
    {
        return t('Oprette hex liste punkt');
    }

    public function save($data)
    {
        parent::save($data);
    }
	
}
