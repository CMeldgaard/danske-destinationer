<?php

defined('C5_EXECUTE') or die(_("Access Denied."));
$al = Loader::helper('concrete/asset_library');
$bf = null;

if ($controller->getFileID() > 0) { 
	$bf = $controller->getFileObject();
}

$fp = FilePermissions::getGlobal();
$tp = new TaskPermission();
?>

<div class="form-group">
	<label class="control-label"><?php echo t('Image')?></label>
	<?php echo $al->image('ccm-b-image', 'fID', t('Choose Image'), $bf, $args);?>
</div>

<div class="form-group">
    <label class="control-label" for="field1"><?=t('Overskrift')?></label>
    <input type="text" class="form-control" name="headline" value="<?php echo $headline?>">
</div>

<div id="redactor-edit-content"><?php echo $controller->getContentEditMode()?></div>
<textarea style="display: none" id="redactor-content" name="content"><?php echo $controller->getContentEditMode()?></textarea>

<script type="text/javascript">
var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Loader::helper('validation/token')->generate('editor')?>";
$(function() {
    $('#redactor-edit-content').redactor({
        minHeight: '300',
        'concrete5': {
            filemanager: <?php echo $fp->canAccessFileManager()?>,
            sitemap: <?php echo $tp->canAccessSitemap()?>,
            lightbox: true
        },
        'plugins': [
            'fontcolor', 'concrete5inline', 'concrete5', 'underline'
        ]
    });
});
</script>
