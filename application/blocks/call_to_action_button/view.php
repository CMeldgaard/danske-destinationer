<?php defined('C5_EXECUTE') or die(_("Access Denied.")) ?>

<a class="btn btn-<?php echo $colour ?>" href="<?php echo $linkURL ?>" role="button" <?php if ($isExt=='1') echo 'rel="nofollow" target="_blank"' ?>><?php echo $text ?></a>
