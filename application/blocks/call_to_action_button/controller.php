<?php

namespace Application\Block\CallToActionButton;

use Core;
use Loader;
use Page;
use Concrete\Core\Block\BlockController;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends BlockController
{
	protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 550;
    protected $btTable = "btCallToActionButton";
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btWrapperClass = 'ccm-ui';
    protected $btExportPageColumns = array('internalLinkCID');

    public function getBlockTypeName()
    {
        return t('knap');
    }

    public function validate($data)
    {
        $e = Core::make('error');
        if (!$data['text']) {
            $e->add(t('Du skal skrive en knap tekst'));
        }

        return $e;
    }

    public function getBlockTypeDescription()
    {
        return t('Tilføje call to action knapper');
    }
	
	function getExternalLink() {return $this->externalLink;}
    function getInternalLinkCID() {return $this->internalLinkCID;}
    function getLinkURL() {
        if (!empty($this->externalLink)) {
            return $this->externalLink;
        } else if (!empty($this->internalLinkCID)) {
            $linkToC = Page::getByID($this->internalLinkCID);
            return (empty($linkToC) || $linkToC->error) ? '' : Loader::helper('navigation')->getLinkToCollection($linkToC);
        } else {
            return '';
        }
    }
	
	    public function view() {

        $this->set('linkURL',$this->getLinkURL());
		if (!empty($this->externalLink)) {
            $this->set('isExt','1');
        }
    }

    public function save($args) {

        switch (intval($args['linkType'])) {
            case 1:
                $args['externalLink'] = '';
                break;
            case 2:
                $args['internalLinkCID'] = 0;
                break;
            default:
                $args['externalLink'] = '';
                $args['internalLinkCID'] = 0;
                break;
        }
        unset($args['linkType']); //this doesn't get saved to the database (it's only for UI usage)
        parent::save($args);
    }
}
