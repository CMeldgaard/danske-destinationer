<?php
namespace Application\Block\Members;

use Concrete\Core\Block\BlockController;
use FileSet;
use Loader;
use Core;
use Page;

class Controller extends BlockController
{

    protected $btTable = "btMembers";
    protected $btInterfaceWidth = "350";
    protected $btInterfaceHeight = "240";
    protected $btDefaultSet = 'basic';

    public function getBlockTypeName()
    {
        return t('Nye medlemmer');
    }

    public function getBlockTypeDescription()
    {
        return t('Tilføj ny række medlemmer');
    }

    public function save($data)
    {
        parent::save($data);
    }
}
