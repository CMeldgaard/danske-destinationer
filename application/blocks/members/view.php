<?php 
defined('C5_EXECUTE') or die("Access Denied.");

$c = Page::getCurrentPage();

?>	

<div class="col-md-4 col-sm-6 members">
<?php
	$a = new Area('Medlem-Venstre-'.$bID.' logo');
	$a->display($c);
?>
	<?php
	$a = new Area('Medlem-Venstre-'.$bID.'  tekst');
	$a->display($c);
?>
	<?php
	$a = new Area('Medlem-Venstre-'.$bID.'  link');
	$a->display($c);
?>
</div>
<div class="col-md-4 col-sm-6 members">
	<?php
	$a = new Area('Medlem-Center-'.$bID.'  logo');
	$a->display($c);
?>
	<?php
	$a = new Area('Medlem-Center-'.$bID.'  tekst');
	$a->display($c);
?>
	<?php
	$a = new Area('Medlem-Center-'.$bID.'  link');
	$a->display($c);
?>
</div>
<div class="col-md-4 col-sm-6 members">
	<?php
	$a = new Area('Medlem-Højre-'.$bID.'  logo');
	$a->display($c);
?>
	<?php
	$a = new Area('Medlem-Højre-'.$bID.'  tekst');
	$a->display($c);
?>
	<?php
	$a = new Area('Medlem-Højre-'.$bID.'  link');
	$a->display($c);
?>
</div>
<br style="clear:both" />