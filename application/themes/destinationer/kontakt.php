﻿<?php
global $u;
?>
<!DOCTYPE html>
<html lang="da">
<head>
<?php Loader::element('header_required')?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Broweser CSS Reset -->
    <link href="<?php echo $view->getThemePath()?>/css/reset.css" rel="stylesheet" />

    <!-- Bootstrap -->
    <link href="<?php echo $view->getThemePath()?>/css/bootstrap.css" rel="stylesheet" />
    <link href="<?php echo $view->getThemePath()?>/css/carousel.css" rel="stylesheet" />

    <!-- CSS Overwrite -->
    <link href="<?php echo $view->getThemePath()?>/css/style.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700,500,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="<?php echo $c->getPageWrapperClass()?>">
    <div class="container menuWrap">
	<?php if ($u -> isLoggedIn ()) {?>
		<div class="userMenu" >
			Hej <?php echo $u->getUserName()?> | <a href="/intranet">Intranet</a>
		</div>
	<?php } ?>
        <div class="menu">
            <a href="/">
                <img src="<?php echo $view->getThemePath()?>/images/logo.png" /></a>
				<?php
				$nav = BlockType::getByHandle('autonav');
				$nav->controller->orderBy = 'display_asc';
				$nav->controller->displayPages = 'top';
				$nav->controller->displaySubpages = 'none';
				$nav->render('templates/responsive_header_navigation');
				?>
        </div>
    </div>
	<?php
	$dir = "application/themes/destinationer/images/banner";
	$images = scandir($dir);
	$i = rand(2, sizeof($images)-1);
	?>
        <div id="frontSlider" class="pageHeader" data-ride="carousel" style="background-position: 50% 0px;">
        <!-- Indicators -->
        <div class="pageHeader-inner" role="listbox">
            <div id="google-map">
                <div class="map-container">
                    <div id="googleMaps" class="google-map-container" style="position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223); width: 100%; height: 600px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="angle">
        <div class="top-angle">
        </div>
        <div class="container">
            <div class="headlineBox">
                <div class="headline">
                    <h1><?php
					$a = new Area('Page Headline');
					$a->display($c);
				?></h1>
                </div>
                <div class="subHeading">
				<?php
					$a = new Area('Page Subheadline');
					$a->display($c);
				?>

                </div>
            </div>
            <div class="row top50">
                <div class="container">
				<?php
					$a = new Area('Top Content Container');
					$a->display($c);
				?>
				<div class="col-md-12 top50">
                        <h2>Bestyrelsen</h2>
                        <div class="col-md-3 col-sm-6 boardMember">
                            <?php
								$a = new Area('Formand billede');
								$a->display($c);
							?>
                            <?php
								$a = new Area('Formand navn');
								$a->display($c);
							?>
                            <h4>Formand</h4>
							<?php
								$a = new Area('Formand tekst');
								$a->display($c);
							?>
                        </div>
                        <div class="col-md-3 col-sm-6 boardMember">
                            <?php
								$a = new Area('Næstformand billede');
								$a->display($c);
							?>
                            <?php
								$a = new Area('Næstformand navn');
								$a->display($c);
							?>
                            <h4>Bestyrelsesmedlem</h4>
							<?php
								$a = new Area('Næstformand tekst');
								$a->display($c);
							?>
                        </div>
                        <div class="col-md-3 col-sm-6 boardMember">
                            <?php
								$a = new Area('Hovedkassere billede');
								$a->display($c);
							?>
                            <?php
								$a = new Area('Hovedkassere navn');
								$a->display($c);
							?>
                            <h4>Hovedkasserer</h4>
							<?php
								$a = new Area('Hovedkassere tekst');
								$a->display($c);
							?>
                        </div>
                        <div class="col-md-3 col-sm-6 boardMember">
                            <?php
								$a = new Area('Bestyrelsesmedlem billede');
								$a->display($c);
							?>
                            <?php
								$a = new Area('Bestyrelsesmedlem navn');
								$a->display($c);
							?>
                            <h4>Bestyrelsesmedlem</h4>
							<?php
								$a = new Area('Bestyrelsesmedlem tekst');
								$a->display($c);
							?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="bottom-angle">
        </div>
    </div>
	<?php
	$dir2 = "application/themes/destinationer/images/pageBreaker";
	$images2 = scandir($dir2);
	$n = rand(2, sizeof($images)-3);
	$style = "background:url(/".$dir."/".$images[$n].") 50% 0 no-repeat fixed;";
	?>
    <div class="pageBreaker" id="pageBreaker" style="<?php echo $style;?>">
        <div class="pageBreakerContent">
                        <?php
			$a = new Area('Breaker Content');
			$a->display($c);
			?>
        </div>
    </div>

    <div class="angle">
        <div class="container">
            <div class="top-angle">
            </div>
			<div class="container"> 
			<?php
					$a = new Area('Bottom Content Container');
					$a->display($c);
				?>
			</div>
        </div>
    </div>

    <!--Scroll top top icon-->
    <div id="scroll-to-top">
        <div class="hex scroll-top">
            <span><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
    </div>

    <div class="footer">
        <div class="footerWrap">
            <img src="<?php echo $view->getThemePath()?>/images/footerLogo.png" />
            <div class="leftText">© Copyright 2014 Danske Destinationer. Design & Udvikling af <a href="http://www.geekmedia.dk" target="_blank">Geek Media</a></div>
            <div class="rightText">DANSKE DESTINATIONER - Vejlsøvej 51, Bygning O, 8600 Silkeborg</div>
        </div>
    </div>
</div>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   
    <script src="<?php echo $view->getThemePath()?>/js/jquery.parallax.js"></script>
	<script src="http://maps.gstatic.com/maps-api-v3/api/js/18/11/intl/da_ALL/main.js" type="text/javascript"></script>
    <script src="<?php echo $view->getThemePath()?>/js/jquery.gmap.js"></script>

    <script type="text/javascript">

        function loadScript(src) {

            var script = document.createElement("script");
            script.type = "text/javascript";
            document.getElementsByTagName("head")[0].appendChild(script);
            script.src = src;
        }

        loadScript('http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initialize');

        function initialize() {
            // Map Markers
            var mapMarkers = [{
                address: "Vejlsøvej 51, 8600 Silkeborg",
                html: "<strong>Danske Destinationer</strong><br>Vejlsøvej 51, Bygning O, 8600 Silkeborg",
                icon: {
                    image: "<?php echo $view->getThemePath()?>/images/mapicon.png",
                    iconsize: [85, 67],
                    iconanchor: [0, 90]
                },
                popup: true
            }];

            // Map Initial Location
            var initLatitude = 56.15433;
            var initLongitude = 9.55905;

            // Map Extended Settings
            var mapSettings = {
                controls: {
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: false,
                    scaleControl: true,
                    streetViewControl: false,
                    overviewMapControl: false
                },
                scrollwheel: false,
                markers: mapMarkers,
                latitude: initLatitude,
                longitude: initLongitude,
                zoom: 16
            };

            var map = $("#googleMaps").gMap(mapSettings);

            // Map Center At
            var mapCenterAt = function (options, e) {
                e.preventDefault();
                $("#googleMaps").gMap("centerAt", options);
            }
        };


    </script>

    <script src="<?php echo $view->getThemePath()?>/js/jquery.nicescroll.min.js"></script>
    <script>
        /*--To top scroll show/hide function--*/
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('#scroll-to-top').fadeIn();
            } else {
                $('#scroll-to-top').fadeOut();
            }
        });

        /* Scroll to top JS controller */
        $('#scroll-to-top').click(function () {
            $("html,body").animate({ scrollTop: 0 }, 1000);
            return false;
        });


        $(document).ready(function () {
            /* Set height of front infoboxes */
            $(".frontBox1").height($(".frontInfoBoxes").height());
            $(".frontBox2").height($(".frontInfoBoxes").height());
            $(".frontBox3").height($(".frontInfoBoxes").height());
            $(".frontBox4").height($(".frontInfoBoxes").height());

            /*--Paralax effect settings--*/
            //.parallax(xPosition, speedFactor, outerHeight) options:
            //xPosition - Horizontal position of the element
            //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
            //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
            $('#frontSlider').parallax("50%", 0.5, true);
            $('#pageBreaker').parallax("50%", 0.5, true);
        })
    </script>

<?php Loader::element('footer_required')?>
 <script src="<?php echo $view->getThemePath()?>/js/carousel.js"></script>
</body>
</html>
