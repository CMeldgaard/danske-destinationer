<?php 
defined('C5_EXECUTE') or die("Access Denied.");

$page = Page::getCurrentPage();


if($page instanceof Page) {
	$cID = $page->getCollectionID();
}

?>	


<?php    if ($displaySetTitle && $filesetname = $controller->getFileSetName()) { ?>
<h3><?php    echo $filesetname; ?></h3>
<?php    } ?>
	
<?php    if (!empty($files)) { ?>	
 	<?php 
	$i = 0;
	foreach($files as $f) {
		$i = $i + 1;
		$fv = $f->getApprovedVersion();
		
		// although the 'title' for a file is used for display,
		// the filename is retreived here so we can always get a file extension
		$filename = $fv->getFileName();
		$description = $fv->getDescription();
		$ext =  pathinfo($filename, PATHINFO_EXTENSION);
		$url = View::url('/download_file', $f->getFileID(),$cID);
 	
		// if you wish to directly link to the file, logging, etc,
		// use instead of the above line:  $url = $fv->getURL();
		
		// if we are overriding the filename (e.g. showing only 1 file)
		if ($titleOverride) {
			$title = $titleOverride;
		} else {
		
			// get the title of the file. This default to the filename on uploading, but can be changed 
			// through the file manager.
			
			$title = $f->getTitle();
			
			// want to always use the filename and not the title?  uncomment line below
			// $title = $filename;
			
			// removes or puts in brackets the file extension
			if ($extension == 'hide') {
				
				if(strlen($title) - strlen($ext) == strrpos($title,$ext)) {
					$title = pathinfo($title, PATHINFO_FILENAME);
				}
				
			} elseif ($extension == 'brackets') {
				
				if(strlen($title) - strlen($ext) == strrpos($title,$ext)) {
					$title = pathinfo($title, PATHINFO_FILENAME);
				}
				
				if ($ext) {
					$title .= ' (' . $ext . ')';
				}
			}
			
			if ($replaceUnderscores) {
				 $title = str_replace('_', ' ', $title);
			}  
			
			if ($uppercaseFirst) {
				$title = ucfirst(strtolower($title));
			}
			
			if ($displaySize) {
		 		$title .= ' - ' . $fv->getSize();
			}


		 	// if you want to add more information about a file (e.g. description, download count)
		 	// look up in the API the functions for a File object and FileVersion object ($f and $fv in above code)
		}
		if ($i>2){	
			if ($i==3){?>
			<br style="clear:both" />
				<h3 style="margin-top: 50px;">Pressearkiv</h3>
			<?php	
			}
		?>
	 
		<div class="col-sm-6 pressArchiv">
		<a href="<?php    echo $url; ?>" target="_blank">
		<img src="<?php echo $view->getThemePath()?>/images/file.png" />
		<span><?php 
		$dh = Core::make('helper/date');
		print $dh->formatDate($fv->getDateAdded()) ?> </span>
		<?php    echo $title; ?>
		</a>
		</div>
		
		
	<?php }else{?>
		
		<div class="col-sm-6 pressMessage">
		<span><?php 
		$dh = Core::make('helper/date');
		print $dh->formatDate($fv->getDateAdded()) ?> </span><br />
		<h2><?php    echo $title; ?></h2>
		<?php echo $description; ?>
		<br /><br />
		<a class="btn btn-trans" href="<?php    echo $url; ?>" target="_blank">
			<img src="<?php echo $view->getThemePath()?>/images/file.png" /> Download som PDF
		</a>
		<br style="clear:both" />
		<br style="clear:both" />
		</div>
		
		
	<?php }	?>
		
		
		
		
		
<?php    }	?>


<?php    }	?>

<?php  if ($pagination): ?>
    <?php  echo $pagination;?>
<?php  endif; ?>


<?php    if (empty($files) && $noFilesMessage) { ?>
<p><?php    echo $noFilesMessage; ?></p>
<?php    } ?>
